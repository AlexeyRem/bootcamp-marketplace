package ru.moscow.alexrem.bootcampmarketplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootcampMarketplaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootcampMarketplaceApplication.class, args);
	}

}
